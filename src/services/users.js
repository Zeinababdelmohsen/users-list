import * as ActionTypes from '../context/actionTypes.users'
import { END_POINT_URL } from '../endpoint'

export function fetchUsers(dispatch, query) {
  dispatch({type: ActionTypes.LOADING, load: true})
	let path = `${END_POINT_URL}/users`

  if (query) {
    path += `?name=${query}`
  }

	fetch(path, {
		method: 'GET',
		headers: {'Content-Type': 'application/json'},
	}).then(res => {
		if (res.status === 200) {
			res.json().then(res => {
				dispatch({
          type: ActionTypes.GET_USERS,
          users: res.data,
          load: false
        })
			})
		} else {
			dispatch({
        type: ActionTypes.FAILED_FETCH,
        msg: 'Failed to load data, please try again later'
      })
		}
	})
  .catch(() => {
    dispatch({
      type: ActionTypes.FAILED_FETCH,
      msg: 'Failed to load data, please try again later' })
  })
}

export function fetchUserProfile(id,dispatch) {
  dispatch({type: ActionTypes.LOADING, load: true})
	let path = `${END_POINT_URL}/users/${id}`
	fetch(path, {
		method: 'GET',
		headers: {'Content-Type': 'application/json'},
	}).then(res => {
		if (res.status === 200) {
			res.json().then(res => {
				dispatch({
          type: ActionTypes.GET_USER_PROFILE,
          user: res.data,
          load: false,
        })
			})
		} else {
			dispatch({
        type: ActionTypes.FAILED_FETCH,
        msg: 'Failed to load data, please try again later',
      })
		}
	})
  .catch(() => {
    dispatch({
      type: ActionTypes.FAILED_FETCH,
      msg: 'Failed to load data, please try again later' })
  })
}

export function fetchUserPosts(id, dispatch) {
  let path = `${END_POINT_URL}/users/${id}/posts`
	fetch(path, {
		method: 'GET',
		headers: {'Content-Type': 'application/json'},
	}).then(res => {
		if (res.status === 200) {
			res.json().then(res => {
				dispatch({
          type: ActionTypes.GET_USER_POSTS,
          posts: res.data,
          load: false
        })
			})
		} else {
			dispatch({
        type: ActionTypes.FAILED_FETCH,
        msg: 'Failed to load data, please try again later'
      })
		}
	})
  .catch(() => {
    dispatch({
      type: ActionTypes.FAILED_FETCH,
      msg: 'Failed to load data, please try again later' })
  })
}

export function fetchUserComments(id, dispatch) {
  let path = `${END_POINT_URL}/posts/${id}/comments`
	fetch(path, {
		method: 'GET',
		headers: {'Content-Type': 'application/json'},
	}).then(res => {
		if (res.status === 200) {
			res.json().then(res => {
				dispatch({
          type: ActionTypes.GET_POST_COMMENTS,
          comments: res.data,
          load: false
        })
			})
		} else {
			dispatch({
        type: ActionTypes.FAILED_FETCH,
        msg: 'Failed to load data, please try again later'
      })
		}
	})
  .catch(() => {
    dispatch({
      type: ActionTypes.FAILED_FETCH,
      msg: 'Failed to load data, please try again later' })
  })
}

export function fetchSinglePost(id, dispatch) {
  let path = `${END_POINT_URL}/posts/${id}`
	fetch(path, {
		method: 'GET',
		headers: {'Content-Type': 'application/json'},
	}).then(res => {
		if (res.status === 200) {
			res.json().then(res => {
				dispatch({
          type: ActionTypes.GET_SINGLE_POST,
          post: res.data,
          load: false
        })
			})
		} else {
			dispatch({
        type: ActionTypes.FAILED_FETCH,
        msg: 'Failed to load data, please try again later'
      })
		}
	})
  .catch(() => {
    dispatch({
      type: ActionTypes.FAILED_FETCH,
      msg: 'Failed to load data, please try again later' })
  })
}

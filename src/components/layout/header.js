import { Layout } from 'antd';
import LogoImg from '../../assets/images/logo.svg';
import { Link } from '@reach/router'

const { Header } = Layout;

function AppHeader() {
  return (
    <Header className='header'>
      <div className="logo">
        <Link to='/'>
          <img className='logo__img' alt="solve-it-logo" src={LogoImg} />
        </Link>
      </div>
    </Header>
  )
}

export default AppHeader;
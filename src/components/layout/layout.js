import { Layout } from 'antd';
import AppHeader from './header';

const { Content } = Layout;

function AppLayout({children}) {
  return (
    <Layout>
      <AppHeader />
      <Content className="site-layout" style={{ padding: '0 50px' }}>
        <div className="site-layout-background" style={{ padding: 24 }}>
          {children}
        </div>
      </Content>
    </Layout>
  )
}

export default AppLayout;
import { Router } from '@reach/router'
import UserProfile from '../pages/user-profile'
import Users from '../pages/users'
import UserPost from '../pages/user-posts';

// Pages

function AppRouter() {
	return (
		<Router>
			<Users path='/' />
			<UserProfile path='/users/:userId' />
			<UserPost path='/user/:userId/post/:postId' />
		</Router>
	)
}

export default AppRouter

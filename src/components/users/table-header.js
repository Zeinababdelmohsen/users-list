
function TableHeader({title, desc}) {
  return (
    <div className='table-header'>
      <h1 className='table-header__title'>{title}</h1>
      <p className='table-header__sub-title'>{desc}</p>
    </div>
  )
}

export default TableHeader

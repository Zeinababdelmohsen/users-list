import { navigate } from '@reach/router';
import { Table } from 'antd';
import { useEffect } from 'react';
import { useUsersState } from '../../context';
import { formatDate } from '../../helpers'
import { fetchUsers } from '../../services/users';

function UsersDataTable() {
  const [{initLoading, users}, dispatch] = useUsersState();

  const columns = [
    {
      title: '#',
      dataIndex: 'id',
      key: '#',
      render: text => text,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: text => text,
    },
    {
      title: 'Email Address',
      dataIndex: 'email',
      key: 'email',
      render: text => text,
    },
    {
      title: 'Gender',
      key: 'gender',
      dataIndex: 'gender',
      render: text => text,
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      render: text => (
        <>
          <i className={text === 'Active' ? 'active-status' : 'inactive-status'}/>
          <span className={text === 'Active' && 'active-text'}>{text}</span>
        </>
      ),
    },
    {
      title: 'Created at',
      key: 'created_at',
      dataIndex: 'created_at',
      render: text => formatDate(text, true),
    },
    {
      title: 'Updated at',
      key: 'updated_at',
      dataIndex: 'updated_at',
      render: text => formatDate(text, true),
    },
  ];

  function goToUserProfile(record,e) {
    navigate(`/users/${record.id}`)
  }

  useEffect(() => {
    fetchUsers(dispatch, false)
  },[dispatch])

  return (
    <Table
      columns={columns}
      dataSource={users}
      loading={initLoading}
      rowKey={record => record.uid}
      pagination={{
        position: 'bottomCenter',
        showQuickJumper: true,
        total: users.length,
        showTotal: (total) => `Showing 5 of ${total} users per page`,
        defaultPageSize: 5
      }}
      size='middle'
      onRow={record => {
        return {
          onClick: () => {goToUserProfile(record)}
        };
      }}
    />
  )
}

export default UsersDataTable
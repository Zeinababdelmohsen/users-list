import { Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import TableHeader from './table-header';
import { fetchUsers } from '../../services/users';
import { useUsersState } from '../../context';

function Search() {
  const [, dispatch] = useUsersState()

  function handleChange(e) {
    const { value } = e.target;
    fetchUsers(dispatch, value)
  }

  return (
    <>
      <TableHeader
        title='User directory'
        desc="Lorem Ipsum dolor sit amet consector"
      />
      <Input
        prefix={<SearchOutlined />}
        onChange={handleChange}
        className="table-header__input"
      />
    </>
  )
}

export default Search

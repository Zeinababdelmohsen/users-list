import { Link } from '@reach/router';
import { Table } from 'antd';
import { useEffect } from 'react';
import { useUsersState } from '../../context';
import { fetchUserPosts, fetchUserComments } from '../../services/users';
import { formatDate } from '../../helpers/index'
import TableHeader from './table-header';

function UserPosts({ userId, postId }) {
  const [{initLoading, userPosts, postComments}, dispatch] = useUsersState();

  const postsCols = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      render: text => text,
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
      render: (text,record) => <Link to={`/user/${userId}/post/${record.id}`}>{text}</Link>,
    },
    {
      title: 'Body',
      dataIndex: 'body',
      key: 'body',
      render: text => text,
    },
  ];

  const commentsCols = [
    {
      title: '#',
      dataIndex: 'id',
      key: 'id',
      render: text => text,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: text => text,
    },
    {
      title: 'Email address',
      dataIndex: 'email',
      key: 'email',
      render: text => text,
    },
    {
      title: 'Body',
      dataIndex: 'body',
      key: 'body',
      render: text => text,
    },
    {
      title: 'Created at',
      dataIndex: 'created_at',
      key: 'created_at',
      render: text => formatDate(text, true),
    },
    {
      title: 'Updated at',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: text => formatDate(text, true),
    },
  ];

  useEffect(() => {
    if (userId && !postId) {
      fetchUserPosts(userId, dispatch)
    }
    if (postId && userId) {
      fetchUserComments(postId, dispatch)
    }
  },[dispatch, postId])

  return (
    <>
        <TableHeader
          title={(postId && userId) ? 'Comments' : 'User directory'}
          desc="Lorem Ipsum dolor sit amet consector"
        />
        <Table
          columns={postId && userId ? commentsCols : postsCols}
          dataSource={postId && userId ? postComments : userPosts}
          loading={initLoading}
          rowKey={record => record.uid}
          pagination
          size='small'
      />
    </>
  )
}

export default UserPosts

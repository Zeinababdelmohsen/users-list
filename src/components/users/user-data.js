import { useEffect } from 'react';
import { useUsersState } from '../../context';
import {fetchUserProfile, fetchSinglePost} from '../../services/users';
import UserAvatar from '../../assets/images/user.svg';
import PostImg from '../../assets/images/image.jpg';
import { formatDate } from '../../helpers';
import { useLocation } from "@reach/router"

function UserData({ userId, postId }) {
  const [{user, userPosts, singlePost}, dispatch] = useUsersState();
  const location = useLocation();

  useEffect(() => {
    fetchUserProfile(userId, dispatch)
    if (location.pathname.includes('post')) {
      fetchSinglePost(postId, dispatch)
    }
  },[postId])

  return (
    <>
      <section className='user'>
        <img className='user__avatar' src={UserAvatar} alt={user.name} />
        <div className='user-meta-data'>
          <h1 className='user__name'>{user.name}</h1>
          <p className='user__email'>{user.email}</p>
          <p
            className={`user__status user__status--${user.status === 'Active' ? 'active' : 'inactive'}`}
          >
            <i className={user.status === 'Active' ? 'active-status' : 'inactive-status'}></i>
            {user.status}
          </p>
        </div>
      </section>
      {location.pathname.includes('post') ? (
        <section className='post'>
          <h2 className='post__title'>{singlePost.title}</h2>
          <span className='post__date'>{formatDate(singlePost.created_at, true)}</span>
          <div className='post-details'>
            <img alt={singlePost.title} src={PostImg} className='post__img' />
            <p className='post__desc'>{singlePost.body}</p>
          </div>
        </section>
      ) : (
        <ul className='user-stats'>
          <li className='user__s'>
            <span>Total <br />no. of posts<br /></span>
            <p className='text-20'>{userPosts && userPosts.length}</p>
          </li>
          <li className='user__s'>
            <span>Current page<br />no. of posts<br /></span>
            <p className='text-20'>{userPosts && userPosts.length}</p>
          </li>
          <li className='user__s'>
            <span>User <br />created at</span><br />
            {formatDate(user.created_at)}
          </li>
          <li className='user__s'>
            <span>User <br />updated at</span><br />
            {formatDate(user.updated_at)}
          </li>
        </ul>
      )}
    </>
  )
}

export default UserData

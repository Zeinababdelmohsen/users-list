/**
 * Users Action Types
 */

 export const GET_USERS = 'GET_USERS'
 export const LOADING = 'LOADING'
 export const FAILED_FETCH = 'FAILED_FETCH'
 export const GET_USER_POSTS = 'GET_USER_POSTS'
 export const GET_USER_PROFILE = 'GET_USER_PROFILE'
 export const GET_POST_COMMENTS = 'GET_POST_COMMENTS'
 export const GET_SINGLE_POST = 'GET_SINGLE_POST'

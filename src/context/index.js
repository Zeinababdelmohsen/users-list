import React, { useContext, useReducer, createContext } from 'react'
import * as ActionTypes from './actionTypes.users'

const usersContext = createContext()

const initState = {
  users: [],
  initLoading: true,
  errorMsg: '',
  userPosts: [],
	user: {},
	postComments: [],
	singlePost: [],
	totalUsers: null,
}

function useUsersState() {
	const context = useContext(usersContext)

	if (!context) {
		throw new Error(
			'useUsersState must be used within a UsersProvider',
		)
	}

	return context
}

function requestReducer(state, action) {
	switch (action.type) {
		case ActionTypes.GET_USERS: {
			return {
				...state,
				initLoading: action.load,
				users: action.users,
			}
		}
    case ActionTypes.FAILED_FETCH: {
			return {
				...state,
				initLoading: false,
				errorMsg: action.msg,
        users: [],
			}
		}
    case ActionTypes.GET_USER_PROFILE: {
			return {
				...state,
				user: action.user,
        initLoading: action.load,
			}
		}
    case ActionTypes.GET_USER_POSTS: {
			return {
				...state,
				initLoading: action.load,
				userPosts: action.posts,
			}
		}
		case ActionTypes.GET_POST_COMMENTS: {
			return {
				...state,
				initLoading: action.load,
				postComments: action.comments,
			}
		}
		case ActionTypes.GET_SINGLE_POST: {
			return {
				...state,
				initLoading: action.load,
				singlePost: action.post,
			}
		}
		default: {
			return state
		}
	}
}

function UsersProvider({ children }) {
	const value = useReducer(requestReducer, initState)

	return (
		<usersContext.Provider value={value}>
			{children}
		</usersContext.Provider>
	)
}

export { useUsersState, UsersProvider }

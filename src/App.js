import './App.less';
import AppLayout from './components/layout/layout';
import AppRouter from './components/router';
import { UsersProvider } from './context';

function App() {
  return (
    <div data-testid='layout'>
      <AppLayout>
        <UsersProvider >
          <AppRouter />
        </UsersProvider>
      </AppLayout>
    </div>
  );
}

export default App;

import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  // render(<App />);
  // const nodeElement = screen.getByRole("layout");
  // expect(nodeElement).toBeTruthy();

  const { getByTestId } = render(<App />)
  expect(getByTestId(/layout/)).toBeInTheDocument();
});

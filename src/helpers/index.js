import { navigate } from "@reach/router"

export function formatDate(date, getTime) {
	const formatedDate = new Date(date)
	const time = formatTime(date)
	const year = formatedDate.getFullYear()

	let month = `${formatedDate.getMonth() + 1}`

	let day = `${formatedDate.getDate()}`

	if (month.length < 2) month = `0${month}`
	if (day.length < 2) day = `0${day}`
	if (getTime) {
		return `${[day, month, year].join('-')} ${time}`
	}

	return [day, month, year].join('-')
}

export function formatTime(date) {
	const formatedTime = new Date(date)
	let hours = formatedTime.getHours();
  let minutes = formatedTime.getMinutes();
	const ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? '0'+minutes : minutes;
	let time = `${hours}:${minutes} ${ampm}`;

	return time
}


export function backBtn(path) {
	navigate(path)
}

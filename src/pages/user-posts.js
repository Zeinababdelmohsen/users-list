import UserData from "../components/users/user-data";
import UserPosts from "../components/users/user-posts";
import { Button, Spin } from 'antd';
import { backBtn } from "../helpers";
import { useUsersState } from "../context";

function UserPost(props) {
  const {userId, postId} = props;
  const [{initLoading}] = useUsersState()

  return (
    <>
      <Spin spinning={initLoading}>
        <Button onClick={() => backBtn(-1)}>Back</Button>
        <hr className='divider' />
        <UserData userId={userId} postId={postId} />
        <hr className='divider' />
        <UserPosts postId={postId} userId={userId} />
      </Spin>
    </>
  )
}

export default UserPost

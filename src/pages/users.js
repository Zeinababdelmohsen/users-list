import Search from "../components/users/search";
import UsersDataTable from "../components/users/table";

function Users() {
  return (
    <>
      <Search />
      <UsersDataTable />
    </>
  )
}

export default Users
import UserData from "../components/users/user-data";
import UserPosts from "../components/users/user-posts";
import { Button, Spin } from 'antd';
import { backBtn } from "../helpers";
import { useUsersState } from "../context";

function UserProfile(props) {
  const {userId} = props;
  const [{initLoading}] = useUsersState()

  return (
    <>
      <Spin spinning={initLoading}>
        <Button onClick={() => backBtn(-1)}>Back</Button>
        <hr className='divider' />
        <UserData userId={userId} />
        <hr className='divider' />
        <UserPosts userId={userId} />
      </Spin>
    </>
  )
}

export default UserProfile
